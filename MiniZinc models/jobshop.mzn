%-----------------------------------------------------------------------------%
% jobshop.mzn
% vim: ft=zinc ts=4 sw=4 et
% Ralph Becket
% Tue May 29 10:48:58 EST 2007
%
% The job-shop scheduling problem.
% --------------------------------
%
% A job shop has some machines, each performing a different operation.
% There are some jobs to be performed.
% A job is a sequence of tasks.
% A task involves processing by a single machine for some duration.
% A machine can operate on at most one task at a time.
% Tasks cannot be interrupted.
%
% The goal is to schedule each job to minimise the finishing time.
%
%-----------------------------------------------------------------------------%

%-----------------------------------------------------------------------------%
% Model parameters.
%

int: n_machines;                        % The number of machines.
int: n_jobs;                            % The number of jobs.
int: n_tasks_per_job = n_machines;      % Each job has one task per machine.
set of int: jobs = 1..n_jobs;
set of int: tasks = 1..n_tasks_per_job;

    % job_task_machine[j, k] is the machine required by task k of job j.
    %
array [jobs, tasks] of 0..(n_machines-1): job_task_machine;

    % job_task_duration[k, k] is the duration of task k of job j.
    %
array [jobs, tasks] of int: job_task_duration;

    % minimal/maximal duration : bounds on end time
    %
int: min_duration = 
    max([sum([job_task_duration[i, j] | j in tasks]) | i in jobs]);

int: max_duration = sum([job_task_duration[i, j] | i in jobs, j in tasks]);

%-----------------------------------------------------------------------------%
% Model variables.
%

    % The start time of each job task.
    %
array [jobs, tasks] of var 0.. max_duration: job_task_start;

    % The finishing time is the time of the last task to complete.
    %
var min_duration..max_duration: t_end;



%-----------------------------------------------------------------------------%
% Constraints.
%

    % Sanity check: tasks cannot take a negative amount of time.
    %
constraint
    forall ( j in jobs, k in tasks ) (
        job_task_duration[j, k]  >=  0
    );

    % Each job task must complete before the next.
    %
constraint
    forall ( j in jobs, k in 1..(n_tasks_per_job - 1) ) (
        job_task_start[j, k] + job_task_duration[j, k]  <=
            job_task_start[j, k + 1]
    );

    % The first job task can start no earlier than time step 0.
    %
constraint
    forall ( j in jobs ) (
        0  <=  job_task_start[j, 1]
    );

    % Tasks on the same machine cannot overlap.
    %
constraint
    forall (
        ja in jobs,
        jb in (ja + 1)..n_jobs,
        ka, kb in tasks
    ) (
        % (N.B.: if-then-elses flatten somewhat faster than implications.)
        if
           job_task_machine[ja, ka]  =  job_task_machine[jb, kb]
        then
            no_overlap( 
                job_task_start[ja, ka], job_task_duration[ja, ka],
                job_task_start[jb, kb], job_task_duration[jb, kb]
            )
        else
            true
        endif
    );

predicate no_overlap(var int: t_a, var int: d_a, var int: t_b, var int: d_b)  =
    ( t_a + d_a  <=  t_b )  \/  ( t_b + d_b  <=  t_a );

    % The finishing time must be no earlier than the finishing time
    % of any task.
    %
constraint
    forall ( j in jobs ) (
            job_task_start[j, n_tasks_per_job] +
            job_task_duration[j, n_tasks_per_job]
        <=
            t_end
    );

%-----------------------------------------------------------------------------%
% Objective.
%
annotation heuristic_search(array[jobs, tasks] of var int: x);

solve ::heuristic_search(job_task_start) minimize t_end;

output [
    "job_task_start = ", show(job_task_start), "\n",
    "t_end = ", show(t_end), "\n"
];

%-----------------------------------------------------------------------------%
%-----------------------------------------------------------------------------%

% Storer, Wu, and Vaccari hard 20x10 instance (Table 2, instance 1)
% n_jobs = 20;
% n_machines = 10;
% job_task_machine = array2d(jobs, tasks, [
% 	3, 2, 1, 4, 0, 8, 9, 5, 7, 6, 
% 	2, 0, 4, 3, 1, 7, 5, 9, 6, 8, 
% 	4, 3, 0, 2, 1, 8, 6, 7, 5, 9, 
% 	2, 3, 1, 0, 4, 8, 7, 6, 5, 9, 
% 	4, 3, 1, 0, 2, 9, 5, 8, 7, 6, 
% 	1, 0, 2, 3, 4, 9, 5, 8, 6, 7, 
% 	0, 1, 4, 3, 2, 7, 8, 6, 9, 5, 
% 	4, 2, 0, 3, 1, 5, 8, 6, 9, 7, 
% 	3, 0, 1, 4, 2, 5, 6, 7, 8, 9, 
% 	3, 0, 1, 4, 2, 7, 8, 5, 9, 6, 
% 	4, 1, 3, 2, 0, 5, 7, 6, 9, 8, 
% 	2, 1, 3, 4, 0, 7, 6, 9, 8, 5, 
% 	1, 3, 4, 0, 2, 7, 5, 9, 6, 8, 
% 	0, 3, 1, 2, 4, 7, 5, 6, 8, 9, 
% 	4, 0, 3, 2, 1, 7, 6, 5, 8, 9, 
% 	1, 2, 3, 0, 4, 7, 6, 9, 8, 5, 
% 	0, 4, 2, 1, 3, 8, 7, 5, 9, 6, 
% 	2, 1, 0, 4, 3, 5, 9, 6, 8, 7, 
% 	1, 0, 4, 2, 3, 5, 7, 9, 6, 8, 
% 	1, 2, 3, 0, 4, 7, 5, 9, 8, 6
% ]);
% job_task_duration = array2d(jobs, tasks, [
% 	19, 27, 39, 13, 25, 37, 40, 54, 74, 93, 
% 	69, 30, 1, 4, 64, 71, 2, 84, 31, 8, 
% 	79, 80, 86, 55, 54, 81, 72, 86, 59, 75, 
% 	76, 15, 26, 17, 30, 44, 91, 83, 52, 68, 
% 	73, 87, 74, 39, 98, 100, 43, 17, 7, 77, 
% 	63, 49, 16, 55, 9, 73, 61, 34, 82, 46, 
% 	87, 71, 43, 80, 39, 70, 18, 41, 79, 44, 
% 	70, 22, 73, 62, 64, 25, 19, 69, 41, 28, 
% 	16, 84, 58, 7, 9, 8, 10, 17, 42, 65, 
% 	8, 10, 3, 41, 3, 40, 56, 53, 96, 13, 
% 	62, 60, 64, 12, 39, 2, 64, 87, 21, 60, 
% 	66, 71, 23, 75, 78, 74, 35, 24, 23, 50, 
% 	5, 92, 6, 69, 80, 13, 17, 89, 80, 47, 
% 	82, 84, 24, 47, 93, 85, 34, 73, 28, 91, 
% 	55, 57, 63, 24, 40, 30, 37, 99, 88, 41, 
% 	75, 47, 68, 7, 78, 80, 2, 23, 49, 50, 
% 	91, 25, 10, 21, 94, 6, 59, 84, 75, 70, 
% 	85, 31, 94, 94, 11, 21, 7, 61, 50, 93, 
% 	27, 77, 13, 30, 2, 88, 4, 39, 53, 54, 
% 	34, 12, 31, 24, 24, 16, 6, 88, 81, 11
% ]);