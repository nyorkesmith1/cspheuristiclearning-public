#ifndef __DATACOLLECTOR_CPP_INCLUDED
#define __DATACOLLECTOR_CPP_INCLUDED

#include <gecode/brancher/data-collector.hh>
#include <gecode/brancher/ml-utils.cpp>

DataCollector* DataCollector::instance = NULL;

DataCollector::DataCollector() {}

DataCollector* DataCollector::getInstance() {
    if (instance == NULL) {
        instance = new DataCollector();
    }
    return instance;
}

void DataCollector::saveResults(float probing_duration, float ml_duration, float solve_duration) {
    ofstream results;
    results.open("../../../results.txt", ios::app);
    results << "Probing time: " << (probing_duration / 1000000.0 ) << "s" << endl;
    results << "ML time: " << (ml_duration / 1000000.0 ) << "s" << endl;
    results << "Solving time: " << (solve_duration / 1000000.0 ) << "s" << endl;
    results << "Total runtime: " << (probing_duration + ml_duration + solve_duration) / 1000000.0 << "s" << endl;
    results << "Number of predictions: " << DataCollector::getInstance()->getNumberOfPredictions() << endl;
    results << "Number of data access calls: " << DataCollector::getInstance()->getNumberOfGetNodeCalls() << endl;
    
    results.close();
}

void DataCollector::loadFromSaveFile() {
    cout << "Loading from save file..." << endl;

    ifstream loadFile("save-file.txt");
    string line;
    cout << "[>                                       ]" << "0% \r";
    if(loadFile.is_open()){

        getline(loadFile, line);
        if (line == "Domain size data"){

            dom_size_data.clear();
            while(getline(loadFile, line) && line != "Variable position data") {
                int asset_id = stoi(line);                
                int domainSize;

                getline(loadFile, line);
                istringstream linestream(line);
                
                dom_size_data.push_back(vector<int>());
                
                while(linestream >> domainSize){
                    dom_size_data.at(asset_id).push_back(domainSize);
                }
            }

            cout << "[========>                               ]" << "20% \r";
            
            while(getline(loadFile, line) && line != "Assigned value data") { 
                int asset_id = stoi(line);
                int varpos;

                getline(loadFile, line);
                istringstream linestream(line);
                
                var_data.push_back(vector<int>());
                
                while(linestream >> varpos){
                    var_data.at(asset_id).push_back(varpos);
                }
            }

            cout << "[================>                     ]" << "40% \r";
            
            while(getline(loadFile, line) && line != "Node score data") {
                int asset_id = stoi(line);
                int val;

                getline(loadFile, line);
                istringstream linestream(line);
                
                val_data.push_back(vector<int>());
                
                while(linestream >> val){
                    val_data.at(asset_id).push_back(val);
                }
            }

            cout << "[========================>               ]" << "60% \r";

            while(getline(loadFile, line) && line != "Assignment data") {
                int asset_id = stoi(line);
                float node_score;

                getline(loadFile, line);
                istringstream linestream(line);
                node_scores.push_back(vector<float>());
                
                while(linestream >> node_score){
                    node_scores.at(asset_id).push_back(node_score);
                }
            }

            cout << "[================================>       ]" << "80% \r";
            while(getline(loadFile, line)) {
                getline(loadFile, line);
                int asset_id = stoi(line);
                
                assignments.push_back(vector<vector<int>>());
                
                while(getline(loadFile, line) && line != ""){
                    istringstream linestream(line);
                    int assignment;
                    vector<int> assigned;
                    while(linestream >> assignment){
                        assigned.push_back(assignment);
                    }
                    assignments.at(asset_id).push_back(assigned);
                }
            }
            
            
        } else {
            cout << "File not structured properly." << endl;
        }

    } else {
        cout << "Could not open file." << endl;
    }

    cout << "[=======================================>]" << "100% \r\n";
    cout << "Loading done!" << endl;
}

void DataCollector::saveToSaveFile() {
    cout << "Saving to save file..." << endl;
    ofstream saveFile;
    // Overwrite the save file
    saveFile.open("save-file.txt", ios::out);
   
    if (saveFile.is_open()){
        int number_of_assets = dom_size_data.size();
        cout << "[>                                       ]" << "0% \r";

        saveFile << "Domain size data" << endl;    
        for(int a = 0; a < number_of_assets; ++a) {
            saveFile << a << endl;
            for(int i = 0; i < dom_size_data.at(a).size(); ++i){
                saveFile << dom_size_data.at(a).at(i) << " ";
            }
            saveFile << endl;
        }

        cout << "[==========>                             ]" << "25% \r";
        saveFile << "Variable position data" << endl;
        for(int a = 0; a < number_of_assets; ++a) {
            saveFile << a << endl;
            for(int i = 0; i < var_data.at(a).size(); ++i){
                saveFile << var_data.at(a).at(i) << " ";
            }
            saveFile << endl;
        }

        cout << "[====================>                   ]" << "50% \r";
        saveFile << "Assigned value data" << endl;
        for(int a = 0; a < number_of_assets; ++a) {
            saveFile << a << endl;
            for(int i = 0; i < val_data.at(a).size(); ++i){
                saveFile << val_data.at(a).at(i) << " ";
            }
            saveFile << endl;
        }

        cout << "[==============================>         ]" << "75% \r";
        saveFile << "Node score data" << endl;
        for(int a = 0; a < number_of_assets; ++a) {
            saveFile << a << endl;
            for(int i = 0; i < node_scores.at(a).size(); ++i){
                saveFile << node_scores.at(a).at(i) << " ";
            }
            saveFile << endl;
        }   

        saveFile << "Assignment data" << endl;
        for(int a = 0; a < number_of_assets; ++a) {
            saveFile << endl << a << endl;
            for(int i = 0; i < assignments.at(a).size(); ++i){
                for(int j = 0; j < assignments.at(a).at(i).size(); ++j){
                    saveFile << assignments.at(a).at(i).at(j) << " ";
                }
                saveFile << endl;
            }
            saveFile << endl;
        }   
        saveFile.close();
        cout << "[=======================================>]" << "100% \r\n";
        cout << "Saving done!" << endl;
    } else {
        cout << "Could not open file." << endl;
    }
}

void DataCollector::clearSavefile(){} // To be implemented

int DataCollector::computeAssetId(int asset_id) {
    int a_id = 0;
    bool includes = false;
    for(int i = 0; i < assets.size(); ++i){
        if(assets.at(i) == asset_id){
            a_id = i;
            includes = true;
            break;
        }
    }

    if(!includes){
        a_id = assets.size();
        assets.push_back(asset_id);
    }

    if ((a_id + 1) > dom_size_data.size()){
        dom_size_data.push_back(vector<int>());
        var_data.push_back(vector<int>());
        val_data.push_back(vector<int>());
        node_scores.push_back(vector<float>());
        assignments.push_back(vector<vector<int>>());
    }

    return a_id;
}

void DataCollector::storeNode(int asset_id, int dom_size, int pos, int val, double node_score) {
    int a_id = computeAssetId(asset_id);

    dom_size_data.at(a_id).push_back(dom_size);
    var_data.at(a_id).push_back(pos);
    val_data.at(a_id).push_back(val);
    node_scores.at(a_id).push_back(node_score);
}

void DataCollector::storeAndComputeNode(int asset_id, vector<int> assigned, ViewArray<Int::IntView> var_array, int pos, int val) {
    int a_id = computeAssetId(asset_id);
    
    vector<int> dom_sizes = dom_size_data.at(a_id);
    int data_size = dom_sizes.size();

    int current_dom_size = MLUtils::computeDomainSize(var_array);
    dom_size_data.at(a_id).push_back(current_dom_size);
    var_data.at(a_id).push_back(pos);
    val_data.at(a_id).push_back(val);
    node_scores.at(a_id).push_back(0.0);

    if(assignments.at(a_id).size() > 0) {
        vector<vector<int>> current_assignments = assignments.at(a_id);

        for(int i = 0; i < current_assignments.size(); ++i) {
            if(current_assignments.at(i) == assigned){
                node_scores.at(a_id).at(i) = MLUtils::computeImpact(dom_sizes.at(i), current_dom_size);
            }  
        }
    }

    assigned.push_back(val);
    assignments.at(a_id).push_back(assigned);
}

double DataCollector::getNodeScore(int dom_size, int var, int val) {
    number_of_get_node_calls++;
    int number_of_assets = dom_size_data.size();

    for(int i = 0; i < number_of_assets; ++i){
        int data_size = dom_size_data.at(i).size();

        for(int j = 0; j < data_size; ++j){
            if(dom_size == dom_size_data.at(i).at(j) && var == var_data.at(i).at(j) && val_data.at(i).at(j)){
                return node_scores.at(i).at(j);
            }
        }
    }

    return -1.0;
}

void DataCollector::print() {
    int node_size = 0;
    cout << endl;
    cout << "Data print from the DataCollector: {" << endl;
    cout << endl;
    for(int a = 0; a < var_data.size(); ++a){

        for(int i = 0; i < var_data.at(a).size(); ++i){
                cout << "  Assignments { ";
                for(int j = 0; j < assignments.at(a).at(i).size(); ++j){
                    cout << assignments.at(a).at(i).at(j) << " ";
                }

                cout << "} , asset_id: " << assets.at(a) << ", ";
                cout << "domain size: " << dom_size_data.at(a).at(i) << ", pos: " << var_data.at(a).at(i);
                cout << ", val: " << val_data.at(a).at(i) << ", node score: " << node_scores.at(a).at(i) << endl;
        }
        node_size += var_data.at(a).size();
    }

    for(int i = 0; i < assets.size(); ++i){
        cout << " Asset: " << assets.at(i) << endl;
    }
    
    cout << endl;
    cout << "  Dom_data size: " << dom_size_data.size() << endl;
    cout << "  Var_data size: " << var_data.size() << endl;
    cout << "  Total nodes stored: " << -1 << endl;
    cout << "}";
}

int DataCollector::getNumberOfPredictions() {
    return number_of_predictions;
}

int DataCollector::getNumberOfGetNodeCalls() {
    return number_of_get_node_calls;
}

int DataCollector::runSVR() {
    PyObject *pModule, *pName, *pFunc;
    PyObject *pArgs;

    number_of_predictions = 0;

    Py_Initialize();
    // Execute python file with machine learning
    pName = PyUnicode_DecodeFSDefault("ml_model");

    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule != NULL) {
        // Execute function 'printTest'
        pFunc = PyObject_GetAttrString(pModule, "supportVectorRegression");
        /* pFunc is a new reference */

        if (pFunc && PyCallable_Check(pFunc)) {
            pArgs = PyTuple_New(4);

            PyObject *pDomSizeList = PyList_New(0);
            PyObject *pVarList = PyList_New(0);
            PyObject *pValList = PyList_New(0);
            PyObject *pNodeScoreList = PyList_New(0);
            
            // Setup arguments for python function call
            int number_of_assets = dom_size_data.size();
            for(int a = 0; a < number_of_assets; ++a){
                for(int i = 0; i < dom_size_data[a].size(); ++i){
                    PyList_Append(pDomSizeList, PyLong_FromLong(dom_size_data[a].at(i)));
                    PyList_Append(pVarList, PyLong_FromLong(var_data[a].at(i)));
                    PyList_Append(pValList, PyLong_FromLong(val_data[a].at(i)));
                    PyList_Append(pNodeScoreList, PyFloat_FromDouble(node_scores[a].at(i)));
                }
            }
            

            PyTuple_SetItem(pArgs, 0, pDomSizeList);
            PyTuple_SetItem(pArgs, 1, pVarList);
            PyTuple_SetItem(pArgs, 2, pValList);
            PyTuple_SetItem(pArgs, 3, pNodeScoreList);

            p_ml_model = PyObject_CallObject(pFunc, pArgs);

            // Decrementing reference counters
            Py_DECREF(pArgs);
            Py_DECREF(pDomSizeList);
            Py_DECREF(pVarList);
            Py_DECREF(pValList);
            Py_DECREF(pNodeScoreList);
            
            if (p_ml_model == NULL) {
                Py_DECREF(p_ml_model);
                Py_DECREF(pModule);
                Py_DECREF(pFunc);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
                return 1;
            }
        }
        Py_XDECREF(pFunc);
        Py_DECREF(pModule);
    }
    return 0;
}

double DataCollector::predictSVR(int domSize, int varPos, int val) {
    number_of_predictions++;
    double prediction;
    PyObject *pFunc;
    PyObject *pArgs, *pPredictionList;
    PyObject *pModule;

    pModule = PyImport_Import(PyUnicode_DecodeFSDefault("ml_model"));

    if (pModule != NULL) {
        pFunc = PyObject_GetAttrString(pModule, "predictSVR");

        if (pFunc && PyCallable_Check(pFunc)) {
            pArgs = PyTuple_New(4);
            Py_INCREF(p_ml_model);
            PyTuple_SetItem(pArgs, 0, p_ml_model);
            PyTuple_SetItem(pArgs, 1, PyLong_FromLong(domSize));
            PyTuple_SetItem(pArgs, 2, PyLong_FromLong(varPos));
            PyTuple_SetItem(pArgs, 3, PyLong_FromLong(val));              

            pPredictionList = PyObject_CallObject(pFunc, pArgs);
            prediction = PyFloat_AsDouble(PyList_GetItem(pPredictionList, 0));

            // Decrementing reference counters
            Py_DECREF(pArgs);
            Py_DECREF(pPredictionList);
            
            if (pPredictionList == NULL) {
                Py_DECREF(pPredictionList);
                Py_DECREF(pFunc);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
                return prediction;
            }
        }
        Py_DECREF(pModule);
        Py_XDECREF(pFunc);
    }
    return prediction;
}

int DataCollector::plotResults() {
    double result;
    PyObject *pFunc;
    PyObject *pArgs, *pPredictionList;
    PyObject *pModule;

    pModule = PyImport_Import(PyUnicode_DecodeFSDefault("ml_model"));

    if (pModule != NULL) {
        pFunc = PyObject_GetAttrString(pModule, "printResult");

        if (pFunc && PyCallable_Check(pFunc)) {
            pArgs = PyTuple_New(4);
            PyTuple_SetItem(pArgs, 0, p_ml_model);          

            pPredictionList = PyObject_CallObject(pFunc, pArgs);
            result = PyFloat_AsDouble(PyList_GetItem(pPredictionList, 0));

            // Decrementing reference counters
            Py_DECREF(pArgs);
            Py_DECREF(pPredictionList);
            
            if (pPredictionList == NULL) {
                Py_DECREF(pPredictionList);
                Py_DECREF(pFunc);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
                return result;
            }
        }
        Py_DECREF(pModule);
        Py_XDECREF(pFunc);
    }
    return result;
}


int DataCollector::finalizeML() {
    Py_FinalizeEx();
    return 1;
}

#endif