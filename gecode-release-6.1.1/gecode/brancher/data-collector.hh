#include <iostream>
#include <fstream>
#include <gecode/int.hh>
#include <string>

using namespace Gecode;
using namespace std;

// Singleton data collector to collect data from search nodes in order to compute node scores based on
// domain size, variable choice, and value assigned to the variable.
class DataCollector {

    public:
        // Retrieves the Singleton instance
        static DataCollector* getInstance();
        // Save results to the results file
        void saveResults(float probing_duration, float ml_duration, float solve_duration);
        // Loads data from savefile
        void loadFromSaveFile();
        // Saves data to savefile
        void saveToSaveFile();
        // Clears save file
        void clearSavefile();
        // Stores the information of the node including node score
        void storeNode(int asset_id, int dom_size, int pos, int val, double node_score);
        // Stores the information of the node and computes a node score based on assignments
        void storeAndComputeNode(int asset_id, vector<int> assigned, ViewArray<Int::IntView> varArray, int pos, int val);
        // Retrieves a node score from the collected data
        double getNodeScore(int dom_size, int var, int val);
        // Prints the data stored in this object
        void print();
        // Send all data to a python script to use Machine Learning
        int runSVR();
        // Predict a score based on ml model
        double predictSVR(int domSize, int varPos, int val);
        // Print run time
        int plotResults();
        // Finalizen python calls
        int finalizeML();

        // Experimental data
        // Get number of predictions done given an ML model
        int getNumberOfPredictions();
        // Get number of predictions done given an ML model
        int getNumberOfGetNodeCalls();

    private:
        // Private constructor to prevent creating instances
        DataCollector();
        // Checks whether the asset id is already in the data collection and adds it if it is not
        int computeAssetId(int asset_id);
        // A python object which stores an ML model learned with the runSVR function
        PyObject *p_ml_model;
        // The Singleton instance
        static DataCollector* instance;
        // Each asset represents it's own collection of data (and is typically a different search tree)
        vector<int> assets;

        // Data storage, a vector for each asset/search tree
        vector<vector<vector<int>>> assignments;
        vector<vector<int>> dom_size_data;
        vector<vector<int>> var_data;
        vector<vector<int>> val_data;
        vector<vector<float>> node_scores;   

        // Experimental data
        int number_of_predictions;
        int number_of_get_node_calls;
};