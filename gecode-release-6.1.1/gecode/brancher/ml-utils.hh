#include <gecode/int.hh>

class MLUtils {

    public:
        // Computes the domain size of each variable in the variable array
        static int computeDomainSize(ViewArray<Int::IntView> var_array);
        // Computes the impact based on the domains (impact based search) when assigning value v to variable x
        static float computeImpact(int previous_dom_size, int cur_dom_size);

};