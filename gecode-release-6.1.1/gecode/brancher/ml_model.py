import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm

def supportVectorRegression(domSizeList, varList, valList, nodeScoreList):
    X = []
    y = []
    for i in range(len(domSizeList)):
        X.append([domSizeList[i], varList[i], valList[i]])
        y.append(nodeScoreList[i])

    y = np.nan_to_num(nodeScoreList)

    clf = svm.SVR(gamma='auto')
    clf.fit(X, y)

    return clf;

def predictSVR(svrModel, domSize, var, val):
    return svrModel.predict([[domSize, var, val]]).tolist()
