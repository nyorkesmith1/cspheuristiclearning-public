#include <gecode/brancher/ml-utils.hh>

int MLUtils::computeDomainSize(ViewArray<Int::IntView> var_array) {
    int dom_size = 0;
    for(int i = 0; i < var_array.size(); ++i){
        dom_size += var_array[i].size();
    }
    return dom_size;
}

float MLUtils::computeImpact(int previous_dom_size, int cur_dom_size) {
    return 1 - ((float)cur_dom_size / (float)previous_dom_size);
}