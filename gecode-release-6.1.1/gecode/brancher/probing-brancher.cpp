/*
 *  Authors:
 *    Christian Schulte <schulte@gecode.org>
 *
 *  Copyright:
 *    Christian Schulte, 2008-2019
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <Python.h>
#include <gecode/brancher/data-collector.cpp>

#include <gecode/int.hh>

#include <iostream>
#include <random>

using namespace Gecode;
using namespace std;

class ProbingSearch : public Brancher {
protected:
	ViewArray<Int::IntView> x;
	// Asset id to identify and asset in portfolio search
	int a_id;
	mutable int start;
	// choice definition
	class PosVal : public Choice {
	public:
		int pos; int val;
		PosVal(const ProbingSearch& b, int p, int v)
			: Choice(b, 2), pos(p), val(v) {}
		virtual void archive(Archive& e) const {
			Choice::archive(e);
			e << pos << val;
		}
	};
	
public:
	ProbingSearch(Home home, ViewArray<Int::IntView>& x0, int asset_id)
		: Brancher(home), x(x0), a_id(asset_id), start(0) {}
	static void post(Home home, ViewArray<Int::IntView>& x, int asset_id) {
		(void) new (home) ProbingSearch(home, x, asset_id);
	}
	virtual size_t dispose(Space& home) {
		(void)Brancher::dispose(home);
		return sizeof(*this);
	}
	ProbingSearch(Space& home, ProbingSearch& b, int asset_id)
		: Brancher(home, b), a_id(asset_id), start(b.start) {
		x.update(home, b.x);
	}
	virtual Brancher* copy(Space& home) {
		return new (home) ProbingSearch(home, *this, a_id);
	}
	// status
	virtual bool status(const Space& home) const {
		for (int i = 0; i < x.size(); i++)
			if (!x[i].assigned()) {
				return true;
			}
		//std::cout << "Found a solution in asset " << a << "\n";
		return false;
	}
	// choice
	virtual Choice* choice(Space& home) {
		vector<int> assigned;
		for (int i = 0; true; i++) {
			if (!x[i].assigned()) {
				// Integer problems for now
				int val = x[i].min();
				//cout << "Made a Choice in asset " << a_id << " with domain: " << x[i] << ", var " << i << " and val: " << val << endl;
				// cout << "Domain: " << x << endl;
				random_device rd; 
				mt19937 eng(rd()); 
				uniform_int_distribution<> distr(0, x[i].size()); 
				int random_index = distr(eng);

				//cout << "x[i] size: " << x[i].size() << endl;
				int index = 0;
				for (Int::ViewValues<Int::IntView> j(x[i]); j(); ++j) {
					if(index == random_index){
						val = j.val();
						break;
					}
					index++;
				}

				// Store value of choice
				DataCollector* dc = DataCollector::getInstance();
				dc->storeAndComputeNode(a_id, assigned, x, i, val);

				return new PosVal(*this, i, val);
			} else {
				// cout << "Storing assigned value: " << x[i].val() << endl;
				assigned.push_back(x[i].val());
			}
		}
		GECODE_NEVER;
		return NULL;
	}
	virtual Choice* choice(const Space&, Archive& e) {
		int pos, val;
		e >> pos >> val;
		return new PosVal(*this, pos, val);
	}
	// commit
	virtual ExecStatus commit(Space& home,
		const Choice& c,
		unsigned int a) {
		// cout << "Committing in asset " << a_id << " ";
		// print(home, c, a, cout);
		// cout << "\n";

		const PosVal& pv = static_cast<const PosVal&>(c);
		int pos = pv.pos, val = pv.val;
		if (a == 0)
			return me_failed(x[pos].eq(home, val)) ? ES_FAILED : ES_OK;
		else
			return me_failed(x[pos].nq(home, val)) ? ES_FAILED : ES_OK;
	}
	// print
	virtual void print(const Space& home, const Choice& c,
		unsigned int a,
		ostream& o) const {
		const PosVal& pv = static_cast<const PosVal&>(c);
		int pos = pv.pos, val = pv.val;
		if (a == 0)
			o << "x[" << pos << "] = " << val;
		else
			o << "x[" << pos << "] != " << val;
	}
};
void probing_search(Home home, const IntVarArgs& x, int asset_id) {
	if (home.failed()) return;
	ViewArray<Int::IntView> y(home, x);
	ProbingSearch::post(home, y, asset_id);
}