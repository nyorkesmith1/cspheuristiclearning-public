/*
 *  Authors:
 *    Christian Schulte <schulte@gecode.org>
 *
 *  Copyright:
 *    Christian Schulte, 2008-2019
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <Python.h>
#include <gecode/int.hh>
#include <gecode/brancher/data-collector.cpp>

#include <iostream>
#include <chrono>

using namespace Gecode;
using namespace std;

class HeuristicSearch : public Brancher {
protected:
	ViewArray<Int::IntView> x;
	mutable int start;
	// Asset id to identify and asset in portfolio search
	int a;
	class PosVal : public Choice {
	public:
		int pos; int val;
		PosVal(const HeuristicSearch& b, int p, int v)
			: Choice(b, 2), pos(p), val(v) {}
		virtual void archive(Archive& e) const {
			Choice::archive(e);
			e << pos << val;
		}
	};
	
public:
	HeuristicSearch(Home home, ViewArray<Int::IntView>& x0, int asset_id)
		: Brancher(home), x(x0), a(asset_id), start(0) {}
	static void post(Home home, ViewArray<Int::IntView>& x, int asset_id) {
		(void) new (home) HeuristicSearch(home, x, asset_id);
	}
	virtual size_t dispose(Space& home) {
		(void)Brancher::dispose(home);
		return sizeof(*this);
	}
	HeuristicSearch(Space& home, HeuristicSearch& b, int asset_id)
		: Brancher(home, b), a(asset_id), start(b.start) {
		x.update(home, b.x);
	}
	virtual Brancher* copy(Space& home) {
		return new (home) HeuristicSearch(home, *this, a);
	}
	// status
	virtual bool status(const Space& home) const {
		for (int i = start; i < x.size(); i++)
			if (!x[i].assigned()) {
				start = i; 
				return true;
			}
		//std::cout << "Found a solution in asset " << a << "\n";
		return false;
	}
	// choice
	virtual Choice* choice(Space& home) {
		int p = start;
		for (int i = start; i < x.size(); i++) {
			if (!x[i].assigned()) {
				p = i;
				//cout << "Made a Choice with domain: " << x[i] << ", var " << i << " and val: " << val << endl;
				//cout << "Domain: " << x << endl;
				
				// Integer problems and value selection for now
				int val = x[p].max();
				double max_score = 0.0;
				DataCollector* dc = DataCollector::getInstance();
				
				for (Int::ViewValues<Int::IntView> j(x[p]); j(); ++j) {
					auto ml_prediction_begin = std::chrono::high_resolution_clock::now();
					int int_val = j.val();
					int dom_size = MLUtils::computeDomainSize(x);
					double score = dc->getNodeScore(dom_size, p, int_val);
					bool madePred = false;

					if(score <= 0.0) {
						score = dc->predictSVR(dom_size, p, int_val);
						madePred = true;
						dc->storeNode(a, dom_size, p, int_val, score);
					}

					auto ml_prediction_end = std::chrono::high_resolution_clock::now();

					float ml_prediction_duration = std::chrono::duration_cast<std::chrono::microseconds>( ml_prediction_end - ml_prediction_begin ).count();
					//std::cout << "Make pred: " << madePred << ", time: " << (ml_prediction_duration / 1000000.0 ) << "s" << std::endl;

					if(score > max_score) {
						max_score = score;
						val = int_val;
					}
				}


				//cout << "Picked value: " << val << " for val pos: " << p << " with node_score: " << max_score << endl;

				return new PosVal(*this, p, val);
			}
		}
		GECODE_NEVER;
		return NULL;		
	}
	virtual Choice* choice(const Space&, Archive& e) {
		int pos, val;
		e >> pos >> val;
		return new PosVal(*this, pos, val);
	}
	// commit
	virtual ExecStatus commit(Space& home,
		const Choice& c,
		unsigned int a) {
		//cout << "Committing ";
		//print(home, c, a, cout);
		//cout << "\n";

		const PosVal& pv = static_cast<const PosVal&>(c);
		int pos = pv.pos, val = pv.val;
		if (a == 0)
			return me_failed(x[pos].eq(home, val)) ? ES_FAILED : ES_OK;
		else
			return me_failed(x[pos].nq(home, val)) ? ES_FAILED : ES_OK;
	}
	// print
	virtual void print(const Space& home, const Choice& c,
		unsigned int a,
		ostream& o) const {
		const PosVal& pv = static_cast<const PosVal&>(c);
		int pos = pv.pos, val = pv.val;
		if (a == 0)
			o << "x[" << pos << "] = " << val;
		else
			o << "x[" << pos << "] != " << val;
	}
};
void heuristic_search(Home home, const IntVarArgs& x, int asset_id) {
	if (home.failed()) return;
	ViewArray<Int::IntView> y(home, x);
	HeuristicSearch::post(home, y, asset_id);
}