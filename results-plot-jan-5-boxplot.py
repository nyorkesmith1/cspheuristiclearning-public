import numpy as np
import matplotlib.pyplot as plt

# RUNTIME
N = 5
labels = ('RCPSP 00', 'RCPSP 06', 'RCPSP 07', 'RCPSP 09')

prob00_av = (150)
gc00 = (37.0335)
hr00 = (39846.4, 42413.7, 48520.9, 49191, 47728.9, 47255.7, 50200.5, 48292.4, 46743.9, 38569.2)
hr00_av = sum(hr00) / len(hr00)

prob02 = (0.016169, 1.07053, 0.065991, 0.067843, 0.133976, 0.036709, 0.029668, 0.027745, 0.043095, 0.117392)
prob02_av = sum(prob02) / len(prob02)
gc02 = (0.001062)
hr02 = (0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
hr02_av = sum(hr02) / len(hr02)

prob03 = (0.016542, 0.037182, 0.034955, 0.030734, 0.031747, 0.025241, 0.03209, 0.042629, 0.036273)
prob03_av = sum(prob03) / len(prob03)
gc03 = (0.002921)
hr03 = (0, 0, 0, 0, 0, 0, 0, 0, 0)
hr03_av = 0

prob06 = (150)
prob06_av = 150
gc06 = (0.034046)
hr06 = (118675, 118796, 118035, 118207, 117468, 116801, 118157, 117947, 100504, 101269)
hr06_av = sum(hr06) / len(hr06)

prob07 = (150)
prob07_av = 150
gc07 = (0.071105)
hr07 = (0.471527, 0.298825, 0.291413, 0.296975, 0.298046, 0.296979, 0.277035, 0.277373, 0.281913, 0.325087)
hr07_av = sum(hr07) / len(hr07)

prob09 = (150)
prob09_av = 150
gc09 = (0.012413)
hr09 = (6343.24, 6656.26, 6778.46, 6343.18, 6101.97, 6223.78, 6329.86, 6493.41, 6247.79, 6150.53)
hr09_av = sum(hr09) / len(hr09)


gecode = (gc00, gc02, gc03, gc06, gc07, gc09)
probing = (prob00_av, prob02_av, prob03_av, prob06_av, prob07_av, prob09_av)
hrlearner = (hr00_av, hr02_av, hr03_av, hr06_av, hr07_av, hr09_av)

ind = np.arange(N)    # the x locations for the groups
#width = 0.35       # the width of the bars: can also be len(x) sequence

# p1 = plt.bar(ind, gecode, width)
# p2 = plt.bar(ind, heuristicLearner, width)

p001 = plt.boxplot([hr00, hr06, hr07, hr09])


plt.ylabel('Runtime (seconds)')
plt.title('RCPSP: different probing times ')
plt.xticks(ind + 0.5, labels)
plt.yscale('log')

# plt.yticks('log')
#plt.legend((p001[0], p002[0], p003[0]), ('Gecode', 'Heuristic Search 0.2 (RF + Impact)', 'Probing'))

plt.show()

### BOXPLOT DATA

# ind = np.arange(1)
# box = plt.boxplot(hr00)
# plt.ylabel('Runtime (seconds)')
# plt.title('RCPSP: different probing times ')
# plt.xticks(ind, ('RCPSP 00'))
# plt.yscale('log')


# plt.show


# NODES
gc00_nod = (2807467)
prob00 = (214781, 202012, 211898, 195495, 199948, 203877, 212770, 208272, 191784, 224843)
prob00_av = sum(prob00) / len(prob00)
hr00_nod = (1575224, 1575224, 1575230, 1575232, 1575230, 1575226, 1575226, 1575224, 1575224, 1575230)
hr00_nod_av = sum(hr00_nod) / len(hr00_nod)

gc02_nod = (96)
prob02 = (433, 15502, 881, 1170, 1916, 276, 199, 184, 332, 1485)
prob02_av = sum(prob02) / len(prob02)
hr02_nod = (0, 0, 0, 0, 0, 0, 0, 0, 0)
hr02_nod_av = sum(hr02_nod) / len(hr02_nod)

gc03_nod = (62)
prob02 = (207, 293, 267, 242, 254, 207, 252, 323, 301)
prob02_av = (sum(prob02) / len(prob02))
hr02_nod = (0, 0, 0, 0, 0, 0, 0, 0, 0)
hr02_nod_av = 0

gc06_nod = (245)
prob06 = (98228, 105270, 108713, 114390, 110824, 107365, 120815, 113052, 120138, 125029)
prob06_av = sum(prob06) / len(prob06)
hr06_nod = 1338260 
hr06_nod_av = 1338260

gc07_nod = (495)
prob07 = (100577, 113140, 115010, 114245, 111933, 111710, 111048, 111837, 113141, 129834)
prob07_av = (sum(prob07) / len(prob07))
hr07_nod = (183, 183, 183, 183, 183, 183, 183, 183, 183, 183)
hr07_nod_av = 183

gc09_nod = (669)
prob09 = (133718, 147869, 155684, 131965, 120703, 120437, 133525, 144361, 130962, 123289)
prob09_av = (sum(prob09) / len(prob09))
hr09_nod = (77471, 77471, 77469, 77471, 77471, 77471, 77473, 77471, 77471, 77471)
hr09_nod_av = sum(hr09_nod) / len(hr09_nod)


gecode_nod = (gc00_nod, gc02_nod, gc03_nod, gc06_nod, gc07_nod, gc09_nod)
probing_nod = (prob00_av, prob02_av, prob02_av, prob06_av, prob07_av, prob09_av)
hrlearner_nod = (hr00_nod_av, hr02_nod_av, hr02_nod_av, hr06_nod_av, hr07_nod_av, hr09_nod_av)

ind = np.arange(N)    # the x locations for the groups
#width = 0.40       # the width of the bars: can also be len(x) sequence

# p1 = plt.bar(ind, gecode, width)
# p2 = plt.bar(ind, heuristicLearner, width)

p001 = plt.boxplot(ind - 0.1, gecode_nod)
p002 = plt.boxplot(ind + 0.1, hrlearner_nod)
p003 = plt.boxplot(ind + 0.3, probing_nod)


plt.ylabel('Nodes searched')
plt.title('RCPSP: Nodes per solver')
plt.xticks(ind, labels)
plt.yscale('log')

# plt.yticks('log')
#plt.legend((p001[0], p002[0], p003[0]), ('Gecode', 'Heuristic Search 0.2 (RF + Impact)', 'Probing'))

plt.show()