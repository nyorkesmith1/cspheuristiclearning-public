# CSPHeuristicLearning

This repository contains the source code for the algorithm designed to learn heuristics for constraint optimization problems (COPs). To apply heuristics to COPs the Geocde solver is used and adapted.

# Compilation

For machine learning Python is used and thus python must be installed on your machine, this must be done by downloading the source code and installing it. Even more so, `python-dev` and `python3.7-dev` must be installed as we embed Python into C++.

To be able to run the solver, follow the steps as in the [Gecode manual](https://www.gecode.org/doc-latest/MPG.pdf), but with additional arguments as follows:
`./configure`, optionally with the flag `--enable-debug` for debugging and `--disable-examples` if examples are not what you are looking for (saves compilation time).
To correctly compile all files we need to change to order of the CXX flags in the `Makefile` at lines 1803 and 1803 to: 
`$(DLLPATH) $(LINKALL) \`  
`$(CXXFLAGS) $(GLDFLAGS) $(LINKSTATICQT)`  
Then compile and install. There is also a shortcut for this using the script `./compile-solver`.
`make CXXUSR="-I/usr/local/include/python3.7m -L/usr/local/lib/libpython3.7m.a -lpython3.7m`  
`make install`

# How to run it

## MiniZinc IDE
The solver can be used with the *MiniZinc IDE* or *MiniZinc terminal*. First the source code must be compiled which can be done by following the steps in section 2.6 from the [Gecode manual](https://www.gecode.org/doc-latest/MPG.pdf). *MiniZinc* first compiles its models to *FlatZinc* which can be interpreted by Gecode in the file `fzn-gecode.exe`. This file is located (after compiling) in the `/tools` directory and must be copied to the root folder where the other `.dll` files are located which were produced by compilation.

Then, to add the custom Gecode solver to MiniZinc, follow the steps in the [MiniZinc documentation](https://www.minizinc.org/doc-2.2.0/en/minizinc_ide.html), section 3.2.5.2. Be sure to select the `fzn-gecode.exe` binary.

## Terminal
The `./hr-learner` program can be used to control the system. In here you can select problems to solve or delete save files.
Alternatively you can use the `./run-solver` script in the root folder to run the program. To adjust the minizinc file simply edit this script.

To manually run it (as in the script), direct to the folder `gecode-release-6.1.1/gecode/brancher` and use the following command to solve the compiled MiniZinc model (a Flatzinc file).

`PYTHONPATH=. ../../tools/flatzinc/fzn-gecode ../../../MiniZinc\ models/<minizinc model>.fzn`
