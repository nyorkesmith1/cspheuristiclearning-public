import numpy as np
import matplotlib.pyplot as plt

# RUNTIME
N = 3
labels = ('2.5 min probing', '5 min probing', '10 min probing')

probing = (150, 300, 600)
search1 = (1094.46, 1051.38, 2026.37)
search2 = (1394.81, 2485.52, 1350.63)
search3 = (1348.79, 1589.86, 2716.28)
search4 = (1118.86, 1616.25, 779.096)

ind = np.arange(N)    # the x locations for the groups
#width = 0.35       # the width of the bars: can also be len(x) sequence

# p1 = plt.bar(ind, gecode, width)
# p2 = plt.bar(ind, heuristicLearner, width)

p1 = plt.bar(ind - 0.3, search1, color = 'b', width = 0.1)
p2 = plt.bar(ind - 0.1, search2, color = 'b', width = 0.1)
p3 = plt.bar(ind + 0.1, search3, color = 'b', width = 0.1)
p4 = plt.bar(ind + 0.3, search4, color = 'b', width = 0.1)


plt.ylabel('Runtime (seconds)')
plt.title('RCPSP: different probing times')
plt.xticks(ind, labels)
#plt.yscale('log')

# plt.yticks('log')
#plt.legend((p1[0], p2[0], p3[0], p4[0]), ('Heuristic Search 0.2'))

plt.show()
# NODES
probing1 = (197558, 274577, 408632)
search1 = (1811044, 1847556, 1856641)
probing2 = (189599, 213825, 297099)
search2 = (1860287, 2016852, 1879985)
probing3 = (180746, 279488,434991)
search3 = (180746,2165427,1843065)
probing4 = (217735, 273668,470902)
search4 = (1811445, 1567243,1536394)


ind = np.arange(N)    # the x locations for the groups
#width = 0.40       # the width of the bars: can also be len(x) sequence

# p1 = plt.bar(ind, gecode, width)
# p2 = plt.bar(ind, heuristicLearner, width)

p1 = plt.bar(ind - 0.3, search1, color = 'g', width = 0.1)
p2 = plt.bar(ind - 0.3, probing1, bottom = search1, color = 'r', width = 0.1)
p3 = plt.bar(ind - 0.1, search2, color = 'g', width = 0.1)
p4 = plt.bar(ind - 0.1, probing2, bottom = search2, color = 'r', width = 0.1)
p5 = plt.bar(ind + 0.1, search3, color = 'g', width = 0.1)
p6 = plt.bar(ind + 0.1, probing3, bottom = search3, color = 'r', width = 0.1)
p7 = plt.bar(ind + 0.3, search4, color = 'g', width = 0.1)
p8 = plt.bar(ind + 0.3, probing4, bottom = search4, color = 'r', width = 0.1)

plt.ylabel('Nodes searched')
plt.title('RCPSP: Nodes per solver')
plt.xticks(ind, labels)
#plt.yscale('log')

# plt.yticks('log')
plt.legend((p1[0], p2[0], p3[0], p4[0], p5[0], p6[0], p7[0], p8[0]), ('Heuristic Search 0.2', 'Probing'))

plt.show()