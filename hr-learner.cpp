#include <iostream>
#include <fstream>

using namespace std;

void runLearnerWithInstance() {
    string dataInstance;
    cout << "------------------------------------------" << endl
         << "The learner will be called with: gecode-release-6.1.1/tools/flatzinc/fzn-gecode MiniZinc\ models/<model>.fzn" << endl
         << "Please enter the name of the model and make sure it has a flatzinc file." << endl
         << "------------------------------------------" << endl;
    cin >> dataInstance;
    system(("./run-solver " + dataInstance).c_str());
}

void clearSaveFile() {
    ofstream savefile;
    savefile.open("gecode-release-6.1.1/gecode/brancher/save-file.txt", ofstream::trunc);
    savefile.close();
    cout << "------------------------------------------" << endl;
    cout << "Save file deleted." << endl;
    cout << "------------------------------------------" << endl;
}

int main(int argc, char *argv[]) {
    cout << "------------------------------------------" << endl
         << "Welcome to the heuristic learner." << endl
         << endl
         << "This software adapts Gecode, a contraint programming solver, to learn good decision making." << endl
         << "Firstly a probing phase is run on the problem whereafter search is initiated." << endl
         << "------------------------------------------" << endl;
    
    char option;

    do {
        cout << "Please select one of the following options or type q to exit." << endl
             << endl
             << " 1) Run learner on a data instance" << endl
             << " 2) Clear save file " << endl
             << "------------------------------------------" << endl;
        cin >> option;

        switch(option) {
            case '1': runLearnerWithInstance(); break;
            case '2': clearSaveFile(); break;
            default : break;
        }

    } while(option != 'q');
}


